# William Clark Wordpress Site
Site for wmclark.co.uk

---
CHANGELOG
=========

## Releases
#### 2.0 Initial Release (04/12/2014 - TJ)
- Updated all content & SEO

#### 1.2.1 Changed Stock Products & Finishings (04/12/2014 - TJ)
- Changed Market Products Header to "Market Sectors"
- Change "Market Products" custom post type labels to "Market Sectors"
- Changed product templates to exclude categories
- Removed Finishings custom post type
- **db** Updated content & structure for stock products
- **db** Updated content & Structure for finishings


#### 1.2.0 Updated Content For Home Page (04/12/2014 - TJ)
- Add a little top & bottom margin for mobile sections
- Added contact page content to home page contact section
- Linked home template to other pages
- Removed mobile left align
- **db** Updated Contact page copy
- **db** Updated Footer contact details

#### 1.1.9 Built out contact & order pages (04/12/2014 - TJ)
- Figured out content for these
- Contacts used temporarily for placing an order
- Contact page structure used on "Place an Order" page
- Perfected responsive margins/etc on contact page
- **db** Entered correct content for both pages
- **db** Installed "Simple Custom Post Order" plugin

#### 1.1.8 Made Site Responsive (04/12/2014 - TJ)
- Left aligned mobile header logo
- Removed product Filtering on mobile
- Adjusted formatting for responsiveness

#### 1.1.7 Created Responsive Header (04/12/2014 - TJ)
- Made header responsive
- Took away body padding on mobile
- Added Mobile Slide in menu
- Animated Menu Toggle button between hamburger and x

#### 1.1.6 Converted Finishing & TC page content to Custom Posts (03/12/2014 - TJ)
- Added Custom Post Types for both pages
- Rebuilt QC and Finishing pages to custom post archives & templates
- Built Single templates for all custom post types
- Added breadcrumbs to all pages
- **db** Reorganized all content into custom Post Types
- **db** Enabled Breadcrumbs

#### 1.1.5 Created QC & Finishing Templates (03/12/2014 - TJ)
- Built template for QC and Finishing Pages
- **db** Added Custom Fields for repeatable anchored items on qc and finishing pages
- **db** Added Content
- **db** Built Order Form

#### 1.1.4 Added Contact Page (03/12/2014 - TJ)
- Built contact page template
- **db** Added custom fields for contact page
- **db** Populated Content

#### 1.1.3 Template Refinement (03/12/2014 - TJ)
- Updated bourbon install
- Added all posts sidebar on single news template
- Added gallery to about template
- Coded responsive gallery styesheet
- **db** Installed widgetkit
- **db** Configured gallery
- **db** Uploaded images for gallery

#### 1.1.2 Built About Page Template & Added Plugins (03/12/2014 - TJ)
- Created Page Templates
- Added full width featured image to page template
- Changed list display in product archives
- **db** - Updated Wordpress SEO by Yoast
- **db** - Installed Nelio AB Testing 3.3.3

#### 1.1.1 Bugfixes & Page Templates (03/12/2014 - TJ)
- Fixed bugs caused by ".home section" css
- Finished stock & market products archive templates
- Redesigned product filtering sidebar
- Added "show all" button to product filter sidebar
- **db** - Cleaned up some content

#### 1.1.0 Product Archives & Filtering (02/12/2014 - TJ)
- Built Templates for Stock Products and Market Products
- Integrated mixitup.js for filtering

#### 1.0.9 News Archive & Single Templates (02/12/2014 - TJ)
- Built Templates for News Archive and Singles
- **db** Added News Article

#### 1.0.8 Custom Post Types for Products & News (02/12/2014 - TJ)
- Added Stock Products & Market Products custom post types
- Changed "Posts" references to "Articles"
- Changed "Posts" label to "News"
- Deleted default wordpress themes
- **db** Populated all product content
- **db** Added Product Categories

#### 1.0.7 (01/12/2014 - TJ)
- Further developed footer style
- Implemented fading background images for home page Hero

#### 1.0.6 Typography & Home Page (01/12/2014 - TJ)
- Styled footer
- Increased size of homepage section fonts
- **DB** - Changed Permalink Settings to taxonomy/post-name


#### 1.0.5 Typography & Home Page (01/12/2014 - TJ)
- Added Fonts
- Set Default Typography in bootstrap variables
- Home page build nearing completion
- Built Sections and replaced static content with Custom Field calls. 
- Fixed Navigation styling to correctly center logo
- Updated ACF Pro Plugin
- **DB Changes**
    - Configured Custom Fields for Home Page
    - Added Content for Home Page
    - Disabled Wordpress SEO by Yoast Plugin


#### 1.0.4 Coded Header Structure (30/11/2014 - TJ)
- Built home page markup in front-page.php
- Css Layout completed for homepage
- Footer markup and layout completed

#### 1.0.3 Coded Header Structure (29/11/2014 - TJ)
- Coded out header.php template
- added bootstrap grid classes

#### 1.0.2 Set up content architecture (29/11/2014 - TJ)

- Added register_nav_menus() to functions/theme-setup.php
- Added Options Pages for Header and Footer
- Cleaned up theme image folder
- **DB Changes**
    - Added Pages & Content in Wordpress
    - Configured Navigation Menu
    - Configured Header and Footer Custom Fields
    - Added above to "Site Info" menu tab
    - Entered Content for Site Info


#### 1.0.1 Theme/Plugins Configuration (29/11/2014 - TJ)
- Changed theme name to pbd-wmclark
- Installed plugins & updated to latest versions
-- Advanced Custom Fields Pro
-- Gravity Forms
-- Google Analytics For Wordpress
-- Wordpress SEO
-- Simple Image Sizes

#### 1.0 Installed from Boilerplate (28/11/2014 - TJ)
- Installed Boilerplate
- DeployHQ Integration put off until 01/12/2014
- Boilerplate wp-config not working, replaced with standard wordpress wp-config





