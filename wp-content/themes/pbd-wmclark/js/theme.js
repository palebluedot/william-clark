jQuery( document ).ready(function() {
  var hiddenContent = jQuery( ".head" );
  jQuery( "a.menu-toggle" ).click(
    function( event ){

      // Cancel the default event (this isn't a real link).
      event.preventDefault();

      // Check to see if the content is visible.
      if (hiddenContent.is( ":visible" )){
        // Hide it slowly.
        hiddenContent.slideUp();
        jQuery('.menu-toggle').removeClass('active');

      } else {
        // Show it slowly.
        hiddenContent.slideDown();
        jQuery('.menu-toggle').addClass('active');
      }
    }
  );
});

// jQuery( document ).ready(function() {
//   jQuery( "a.menu-item-has-child").click(
//     function( event ){
//       event.preventDefault();
//     }
//   );
// });


jQuery(function(){

  // Instantiate MixItUp:

jQuery('#Container').mixItUp({
  callbacks: {
    onMixEnd: function(state){
      console.log(state)
    } 
  }
});
});

