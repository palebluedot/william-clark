<h6>Cotton Holland</h6>
100% cotton fabric discreet stay tape, rolled full width or folded on boards

<h6>Cotton Dommette</h6>
A soft, raised 100% cotton fabric interlining for warmth and comfort,

<h6>Cotton Silesia</h6>
A soft, 100% cotton fabric jacket pocketing.
<em>100% cotton fabric press interliner</em> - for re-enforcing small areas in men’s and ladies wear. Not dry cleanable, washable only at 40C.
<em>100% cotton fabric permanent press interliner </em>- for re-enforcing small areas in men’s and ladies wear Dry cleanable and washable at 40C

<h6>Hair Canvas</h6>
Blended hair canvas for chest tailoring coats &amp; suits

<h6>Fusible Interlinings</h6>
<em>Scribble </em>- Medium handle cotton/viscose that’s fusible. An 11-dot polyamide coating for men’s and ladies chest fronts. The large 11 dot is designed to grip and adhere to fibrous jacket fabrics. Dry cleanable.