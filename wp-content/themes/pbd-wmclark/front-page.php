<?php
/**
 * @package WordPress
 */?>

<!-- Call Header Template -->
<?php get_header(); ?>

<!-- Insert Page Body -->



<section class="home-hero">
    <div class="slide"><span>image</span></div>
    <div class="slide"><span>image</span></div>
    <div class="slide"><span>image</span></div>
    <div class="slide"><span>image</span></div>
    <div class="slide"><span>image</span></div>
    <div class="hero-text">
    <h1>The Home of Fine Fabric</h1>
      <?php the_field('heroText'); ?>
    </div>
</section>

<section class="home-about">
  <h2>About</h2>
  <?php the_field('aboutText'); ?>
  <a href="/about" class="read-more">Read More</a>
</section>



<?php if( have_rows('homeLinks') ) : ?>
  <section class="home-nav">
    <?php while( have_rows('homeLinks') ) : the_row(); ?>
    <a class="home-navitem" href="<?php the_sub_field('link'); ?>">
      <?php $attachment_id = get_sub_field('image');
            $size = "full";
            $image = wp_get_attachment_image_src( $attachment_id, $size ); ?>
      
      <img src="<?php echo $image[0]; ?>" />
      <h4 class="home-navitem-title"><?php the_sub_field('title'); ?></h4>
    </a>
    <?php endwhile; ?>
  </section>
<?php endif; ?>

<section class="home-news">
  <h2>News</h2>
  <?php the_field('newsText'); ?>
  <a href="/news" class="read-more">Read News</a>
</section>

    <?php if( have_rows('contactItems', 16) ) : ?>
        <section class="contact-bottom-content">
        <h2>Contact</h2>
            <?php while( have_rows('contactItems', 16) ) : the_row(); ?>
                <div class="contact-item">
                    <h4><?php the_sub_field('title'); ?></h4>
                    <?php the_sub_field('content'); ?>
                </div>
            <?php endwhile; ?>
        </section>
    <?php endif; ?>


<!-- Close Page Body -->

<!-- Call Footer Template -->
<?php get_footer(); ?>