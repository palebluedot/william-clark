<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="page-title"><h1>Quality Control</h1></section>
<section class="row">
    <?php if (have_posts()): ?>
    <div class="news-sidebar">
        <table class="product-filter">
        <thead>
            <tr>

                <td>
                    <h4>Quality Control</h4>
                    <button data-filter="all" class="filter pull-right">show all</button>
                </td>
            </tr>
        </thead>
        <tbody>
     
            <?php if( have_rows('anchoredItems') ) : while( have_rows('anchoredItems') ) : the_row(); 
            $titleClass = get_sub_field('title');
            $titleClassSan = sanitize_html_class( $titleClass);
            ?>
            <tr>
                <td class="filter" data-filter=".<?php echo $titleClassSan ?>">
                    <p><?php the_sub_field('title'); ?></p>
                </td>
            </tr>
            <?php endwhile; endif; ?>

        </tbody>
        </table>
    </div>
    <?php endif; ?>


    <div class="archive-list" id="Container">
            <?php if( have_rows('anchoredItems') ) : while( have_rows('anchoredItems') ) : the_row(); 
            $titleClass= get_sub_field('title');
            $titleClassSan = sanitize_html_class( $titleClass);
            ?>
            <div class="archive-list-article mix <?php echo $titleClassSan ?>">

                <div class="postcontent">

                    <h3><?php the_sub_field('title'); ?></h3>
                    <?php the_sub_field('content'); ?>

                </div><!--/postcontent-->
    
            </div>

        <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>