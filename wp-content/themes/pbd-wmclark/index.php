<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="page-title"><h1>NEWS</h1></section>
<section class="row">
    <?php if (have_posts()): ?>
    <div class="news-sidebar">
        <table class="news-filter">
        <thead>
            <tr>
                <td><h4>Recent Posts</h4></td>
            </tr>
        </thead>
        <tbody> 
            <?php while (have_posts()): the_post(); ?>

                <tr><td><a href="<?php the_permalink(); ?>">
                    <p><?php the_title(); ?></p>
                    <p class="date"><?php echo the_time("F jS, Y"); ?></p>
                </a></td></tr>

            <?php endwhile; ?>
        </tbody>
        </table>
    </div>
    <?php endif; ?>


    <div class="news-list">
        <?php if (have_posts()): while (have_posts()): the_post(); ?>

            <div class="news-list-article">
            <?php if ( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )): ?>
                <div class="title">
                    <header>
                        <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                        </a>
                        <p class="date"><?php echo the_time("F jS, Y"); ?></p>
                    </header>

                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <img src="<?php echo $url; ?>">
                </div>

                <div class="postcontent">
            <?php else: ?>
                <div class="postcontent">
                <header>
                    <a href="<?php the_permalink(); ?>">
                    <h3><?php the_title(); ?></h3>
                    </a>
                    <p class="date"><?php echo the_time("F jS, Y"); ?></p>
                </header>                  
            <?php endif; ?>  
                
                    <?php the_content(__('(continue...)')); ?>
                </div><!--/postcontent-->
    
            </div><!--/post-->

        <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>