<?php
/**
 * @package WordPress
 */
get_header();
if (have_posts()): while (have_posts()): the_post(); ?>
    

    <section class="page-title"><h1><?php the_title(); ?></h1></section>
    

    <?php if ( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )): ?>
    
    <section class="featured-image">
            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <img src="<?php echo $url ?>" alt="">
    </section>

    <?php endif; ?>  
    



    <section class="main-content">
                <div class="postcontent">
                    <?php the_content(__('(continue...)')); ?>
                </div>
    </section>
    <section class="page-gallery">
        <?php echo do_shortcode('[widgetkit id=92]' ); ?>
    </section>




<?php endwhile; else: ?>
    <section class="main-content">
        <?php get_template_part('notfound'); ?>

    </section>
<?php endif; ?>
<?php get_footer(); ?>