<?php
/**
 * @package WordPress
 */
?>

<section class="foot">
    <h1 class="foot-title">William Clark and Sons</h1>
    <div class="foot-address">
      <?php the_field('companyAddress', 'option'); ?>
      <p class="social-link">
        <a href="http://www.twitter.com/irishlinenclark" target="_blank">
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
          </span>
        </a>
    
        <a href="https://www.facebook.com/pages/William-Clark-Sons/398457273643181" target="_blank">
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
          </span>
        </a>
      </p>
    </div>
    <div class="foot-contactinfo">
      <p>T: <?php the_field('companyPhone', 'option'); ?></p>
      <p>E: <a href="mailto:<?php the_field('companyEmail', 'option'); ?>"><?php the_field('companyEmail', 'option'); ?></a> </p>
    </div>
    <div class="foot-spacer"></div>
    <div class="foot-branding">
      
      <?php $attachment_id = get_field('companyLogo', 'option');
      $size = "full";
      $image = wp_get_attachment_image_src( $attachment_id, $size );
      ?>
      
      <img src="<?php echo $image[0]; ?>" />

    </div>
  </div>

</section>

<?php wp_footer(); ?>
</body>
</html>