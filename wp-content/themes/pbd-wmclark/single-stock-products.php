<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="row">
    <?php if (have_posts()): ?>
    <div class="news-sidebar">
    
        <table class="product-filter">
        <thead>
            <tr>
                <td><h4>Other Stock Products</h4></td>
            </tr>
        </thead>
        <tbody> 
            <?php $allPosts = new WP_Query('post_type=stock-products'); ?>
            <?php while ($allPosts->have_posts()): $allPosts->the_post(); ?>
            
                <tr><td><a href="<?php the_permalink(); ?>">
                    <p><?php the_title(); ?></p>
                </a></td></tr>

            <?php endwhile; ?>
        </tbody>
        </table>
    </div>
    <?php endif; ?>


    <div class="news-list">
        <?php if (have_posts()): while (have_posts()): the_post(); ?>

            <div class="news-list-article">
            
            <?php if ( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )): ?>
                <div class="title">
                    <header>
                        
                        <h1><?php the_title(); ?></h1>
                        <?php yoast_breadcrumb('<div id="breadcrumbs">','</div>'); ?>    
                    
                    </header>

                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <img src="<?php echo $url; ?>">
                </div>

                <div class="postcontent">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
            <?php else: ?>

                <div class="postcontent">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
                <header>
                    <a href="<?php the_permalink(); ?>">
                    <h1><?php the_title(); ?></h1>
                    </a>
                    
                </header>                  
            <?php endif; ?>  
                
                    <?php the_content(__('(continue...)')); ?>
                </div><!--/postcontent-->
    
            </div><!--/post-->

        <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>