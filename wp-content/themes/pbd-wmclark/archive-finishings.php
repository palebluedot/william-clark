<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="page-title"><h1>Finishing</h1></section>
<section class="row">
    <?php if (have_posts()): ?>
    <div class="news-sidebar">
        <table class="product-filter">
        <thead>
            <tr>

                <td>
                    <h4>Finishing</h4>
                    <button data-filter="all" class="filter pull-right">show all</button>
                </td>
            </tr>
        </thead>
        <tbody>
     
            <?php if (have_posts()): while (have_posts()): the_post();
            $titleClass = get_the_title();
            $titleClassSan = sanitize_html_class( $titleClass);
            ?>
            <tr>
                <td class="filter" data-filter=".<?php echo $titleClassSan ?>">
                    <p><?php the_title(); ?></p>
                </td>
            </tr>
            <?php endwhile; endif; ?>

        </tbody>
        </table>
    </div>
    <?php endif; ?>


<div class="archive-list" id="Container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
            } ?>
        <?php if (have_posts()): while (have_posts()): the_post();
            $titleClass = get_the_title();
            $titleClassSan = sanitize_html_class( $titleClass);
            ?>
            <div class="archive-list-article mix <?php echo $titleClassSan ?>">
            <?php if ( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )): ?>
                <div class="title">
                    <header>
                        <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                        </a>
                        
                    </header>

                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <img src="<?php echo $url; ?>">
                </div>

                <div class="postcontent">
            <?php else: ?>
                <div class="postcontent">
                <header>
                    <a href="<?php the_permalink(); ?>">
                    <h3><?php the_title(); ?></h3>
                    </a>
                </header>                  
            <?php endif; ?>  
                
                    <?php the_content(__('(continue...)')); ?>
                </div><!--/postcontent-->
    
            </div><!--/post-->

        <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>