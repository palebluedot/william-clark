<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="page-title"><h1>Finishing processes for cotton and linen material</h1></section>
<section class="row">
    <div class="finishing-list" id="Container">
            <?php the_content( ); ?>
    </div>
</section>
<?php get_footer(); ?>