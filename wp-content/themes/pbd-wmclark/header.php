<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title><?php wp_title('|', true, 'right'); ?></title>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    

    <!-- stops mobile zooming, sets size to actual viewport size-->
    <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />

    <?php wp_head(); ?>

</head>

<body <?php body_class( ); ?>>

<!-- container-lg is a full width area -->
<div class="mobile-head">
    <a class="branding" href="/">
    
      <?php $attachment_id = get_field('companyLogo', 'option');
      $size = "full";
      $image = wp_get_attachment_image_src( $attachment_id, $size );
      ?>
      
      <img src="<?php echo $image[0]; ?>" />
    </a>
    <a href="#" class="menu-toggle">
      <span></span>
    </a>
  </div>
<div class="container-fluid">



  



  <section class="head">

        <!-- code for primary navigation in page head -->
        <?php

        $defaults = array(
          'theme_location'  => 'top_right',
          'menu'            => '',
          'container'       => 'div',
          'container_class' => 'menu-wrapper',
          'container_id'    => '',
          'menu_class'      => 'menu-left',
          'menu_id'         => '',
          'echo'            => true,
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        );

        wp_nav_menu( $defaults );

        ?>
        
        <a class="branding" href="/">
        
          <?php $attachment_id = get_field('companyLogo', 'option');
          $size = "full";
          $image = wp_get_attachment_image_src( $attachment_id, $size );
          ?>
          
          <img src="<?php echo $image[0]; ?>" />
        </a>
        
        <?php

        $defaults = array(
          'theme_location'  => 'top_left',
          'menu'            => '',
          'container'       => 'div',
          'container_class' => 'menu-wrapper',
          'container_id'    => '',
          'menu_class'      => 'menu-right',
          'menu_id'         => '',
          'echo'            => true,
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
          'depth'           => 0,
          'walker'          => ''
        );

        wp_nav_menu( $defaults );

        ?>


  </section>