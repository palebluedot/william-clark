<?php



/**
 * Private Posts visible to Subscribers
 * Run once
 */
function be_private_posts_subscribers() {
  $subRole = get_role( 'subscriber' );
    $subRole->add_cap( 'read_private_posts' );
}
add_action( 'init', 'be_private_posts_subscribers' );



/**
 * Remove "Private" from private page titles.
 */
function bfa_remove_word_private($string) {
$string = str_ireplace("private: ", "", $string);
return $string;
}
add_filter('the_title', 'bfa_remove_word_private');
