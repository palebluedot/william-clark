<?php
  /**
  *
  * Create Stock Product type
  * You can copy and paste for multiples, but be sure to change the function name and calling to a new one. 
  *
  **/

  function codex_custom_init1() {
    $labels = array(
      'name'               => 'Stock Products',
      'singular_name'      => 'Stock Product',
      'add_new'            => 'Add Stock Product',
      'add_new_item'       => 'Add New Stock Product',
      'edit_item'          => 'Edit Stock Product',
      'new_item'           => 'New Stock Product',
      'all_items'          => 'All Stock Products',
      'view_item'          => 'View Stock Product',
      'search_items'       => 'Search Stock Products',
      'not_found'          => 'No Stock Products found',
      'not_found_in_trash' => 'No Stock Products found in Trash',
      'parent_item_colon'  => '',
      'menu_name'          => 'Stock Products'
    );

    $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'taxonomies'         => array('category'),
      'rewrite'            => array( 'slug' => 'stock-products' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => 5,
      'menu_icon'          => 'dashicons-portfolio',
      'supports'           => array( 'title', 'editor', 'thumbnail', 'categories' )
    );

    register_post_type( 'stock-products', $args );
  }

  add_action( 'init', 'codex_custom_init1' );

  function codex_custom_init2() {
    $labels = array(
      'name'               => 'Market Sectors',
      'singular_name'      => 'Market Sector',
      'add_new'            => 'Add Market Sector',
      'add_new_item'       => 'Add New Market Sector',
      'edit_item'          => 'Edit Market Product',
      'new_item'           => 'New Market Sector',
      'all_items'          => 'All Market Sectors',
      'view_item'          => 'View Market Sector',
      'search_items'       => 'Search Market Sectors',
      'not_found'          => 'No Market Sectors found',
      'not_found_in_trash' => 'No Market Sectors found in Trash',
      'parent_item_colon'  => '',
      'menu_name'          => 'Market Sectors'
    );

    $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'taxonomies'         => array('category'),
      'rewrite'            => array( 'slug' => 'market-products' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => 5,
      'menu_icon'          => 'dashicons-portfolio',
      'supports'           => array( 'title', 'editor', 'thumbnail', 'categories' )
    );

    register_post_type( 'market-products', $args );
  }

  add_action( 'init', 'codex_custom_init2' );


function codex_custom_init4() {
    $labels = array(
      'name'               => 'Quality Controls',
      'singular_name'      => 'Quality Control',
      'add_new'            => 'Add Quality Control',
      'add_new_item'       => 'Add New Quality Control',
      'edit_item'          => 'Edit Quality Control',
      'new_item'           => 'New Quality Control',
      'all_items'          => 'All Quality Controls',
      'view_item'          => 'View Quality Control',
      'search_items'       => 'Search Quality Controls',
      'not_found'          => 'No Quality Controls found',
      'not_found_in_trash' => 'No Quality Controls found in Trash',
      'parent_item_colon'  => '',
      'menu_name'          => 'Quality Controls'
    );

    $args = array(
      'labels'             => $labels,
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'taxonomies'         => array('category'),
      'rewrite'            => array( 'slug' => 'quality-controls' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => 5,
      'menu_icon'          => 'dashicons-portfolio',
      'supports'           => array( 'title', 'editor', 'thumbnail', 'categories' )
    );

    register_post_type( 'quality-controls', $args );
    flush_rewrite_rules();
  }

  add_action( 'init', 'codex_custom_init4' );

/* ===============================================================
    change "posts" label to "articles"
=============================================================== */
function pbd_change_post_menu_label()
{
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'All News';
    $submenu['edit.php'][10][0] = 'Add Article';
    $submenu['edit.php'][16][0] = 'Article Tags';
}
function pbd_change_post_object_label()
{
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Articles';
    $labels->singular_name = 'Article';
    $labels->add_new = 'Add Article';
    $labels->add_new_item = 'Add New Article';
    $labels->edit_item = 'Edit Article';
    $labels->new_item = 'New Article';
    $labels->view_item = 'View Article';
    $labels->search_items = 'Search Articles';
    $labels->not_found = 'No articles found';
    $labels->not_found_in_trash = 'No articles found in Trash';
}
add_action('init', 'pbd_change_post_object_label');
add_action('admin_menu', 'pbd_change_post_menu_label');

