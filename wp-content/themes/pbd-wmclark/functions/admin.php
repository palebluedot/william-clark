<?php

  /**
  *
  * Change Admin Login Logo
  *
  **/

  add_action("login_head", "client_logo");
  add_filter( 'login_headerurl', 'client_loginlogo_url' );
  function client_logo() {
      echo "
      <style>
      #login { width:350px; padding:4% 0 0; }
      body.login { background:#5f636a; }
      body.login #login h1 a {
          background: url('".get_bloginfo('template_url')."/img/h2020_logo_admin.png') no-repeat scroll center top transparent;
          height: 59px;
          width: 350px;
      }
      .login #nav a, .login #backtoblog a { color:#43c5e4!important; text-shadow:none!important; }
      .wp-core-ui .button-primary { background:#43c5e4!important; border:none; text-shadow:none; }
      </style>
      ";
  }
  function client_loginlogo_url($url) {
    return get_bloginfo('url');
  }


  /* == Disable admin file editing ===================================================== */
  define('DISALLOW_FILE_EDIT', true);


  /* == remove admin bar =============================================================== */
  // add_filter('show_admin_bar', '__return_false');


  /* == Disable WordPress Admin Bar for all users but admins. ========================== */
  show_admin_bar(false);


  /* == Remove WP Upgrade Banner ======================================================= */
  add_action('admin_menu','wphidenag');
  function wphidenag() {
      remove_action( 'admin_notices', 'update_nag', 3 );
  }

  /* == add svg support for media uploader ============================================= */
  function cc_mime_types(  $mimes ){
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter( 'upload_mimes', 'cc_mime_types' );

  /* == custom styles for wysiwyg editor =============================================== */
  // add_editor_style('editor-style.css');


  /**
  *
  * Create Layout Custom Post Type
  *
  **/
  // function theme_layout() {
  //         register_post_type( 'layout', array(
  //                 'labels' => array(
  //                         'name' => 'Layout',
  //                         'singular_name' => 'layout',
  //                 ),
  //                 'public' => false,
  //                 'exclude_from_search' => true,
  //                 'show_ui' => true,
  //                 'show_in_menu' => 'themes.php',
  //                 'supports' => array( 'title' ,'thumbnail', 'editor' ),
  //         ) );
  // }
  // add_action( 'init', 'theme_layout' );


  //OR------------------------

  /* == add additional options pages ========== */
  if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
      'page_title'  => 'Site Info',
      'menu_title'  => 'Site Info',
      'menu_slug'   => 'site-info',
      'capability'  => 'edit_posts',
      'redirect'    => false
    ));
    
    // acf_add_options_sub_page(array(
    //   'page_title'  => 'Header Settings',
    //   'menu_title'  => 'Header',
    //   'parent_slug' => 'sections',
    // ));
    
    // acf_add_options_sub_page(array(
    //   'page_title'  => 'Footer Settings',
    //   'menu_title'  => 'Footer',
    //   'parent_slug' => 'sections',
    // ));
    
  }




  /**
  *
  * Add link directly to edit home page for Single Page Applications
  * BE SURE TO EDIT POST # TO POINT TO HOME PAGE
  *
  **/
  // add_action( 'admin_menu', 'register_my_custom_menu_page' );
  // function register_my_custom_menu_page(){
  //     add_menu_page( 'Edit Homepage', 'Edit Homepage', 'manage_options', 'post.php?post=6&action=edit', '', '', 3 );
  // }



  /* == Removes the default link on attachments  ======================================= */
  // update_option('image_default_link_type', 'none');



  /* == remove funky formatting caused by tinymce advanced ============================= */
  // function fixtinymceadv($text)
  // {
  //     $text = str_replace('<p><br class="spacer_" /></p>','<br />',$text);
  //     return $text;
  // }
  // add_filter('the_content',  'fixtinymceadv');



  /* == Add blog name to title ========================================================= */
  // function pbd_alter_title($title, $sep) {
  //     $title .= get_bloginfo('name');
  //     return $title;
  // }
  // add_filter('wp_title', 'pbd_alter_title', 10, 2);


  