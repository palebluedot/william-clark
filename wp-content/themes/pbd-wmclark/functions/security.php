<?php 

/* == Remove the version number of WP  =============================================== */
remove_action('wp_head', 'wp_generator');

/* == Obscure login screen error messages ============================================ */
function login_obscure()
{
    return '<strong>Sorry</strong>: Information that you have entered is incorrect.';
}
add_filter('login_errors', 'login_obscure');
