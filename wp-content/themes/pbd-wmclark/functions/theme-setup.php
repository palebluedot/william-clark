<?php 


  /* == Queue up all css & js files ==================================================== */
  function pbd_scripts_styles()  

  {   
      // wp_enqueue_script('owl', get_template_directory_uri() . '/js/owl.carousel.min.js',array('jquery'), null, true);
      wp_enqueue_script('mixitup', get_template_directory_uri() . '/js/jquery.mixitup.min.js',array('jquery'), null, true);
      // wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.min.js',array('jquery'), null, true);
      // wp_enqueue_script('maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false',array('jquery'), null, true);
      wp_enqueue_script('pbd_script', get_template_directory_uri() . '/js/theme.js',array('jquery'), null, true);
      wp_enqueue_style('pbd_style', get_template_directory_uri() . '/css/global.css', false, null);
  }
  add_action('wp_enqueue_scripts', 'pbd_scripts_styles');


  /* == register menus ================================================================ */
  
  register_nav_menus( array(
    'top_left' => 'Top Right Navigation',
    'top_right' => 'Top Left Navigation',
  ) );

  /* == add thumbnail and featured image support ======================================= */
  add_theme_support('post-thumbnails');
  
  /**
  *
  * Remove Items from wp_head()
  *
  **/

  remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
  remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
  remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
  remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
  remove_action('wp_head', 'index_rel_link'); // index link
  remove_action('wp_head', 'parent_post_rel_link'); // prev link
  remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
  remove_action('wp_head', 'rel_canonical');
  remove_action('wp_head', 'previous_post_link');
  remove_action('wp_head', 'next_post_link');
  remove_action('wp_head', 'post_permalink');
  remove_action('wp_head', 'the_permalink');
  remove_action('wp_head', 'start_post_rel_link', 10, 0 );
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  //remove_filter('term_description','wpautop');