<?php
/**
 * @package WordPress
 */
get_header(); ?>
<section class="page-title"><h1>Market Sectors</h1></section>
<section class="row">
    <?php if (have_posts()): ?>
    <div class="news-sidebar">
        <table class="product-filter">
        <thead>
            <tr>

                <td>
                    <h4>I'm looking for:</h4>
                    <button data-filter="all" class="filter pull-right">show all</button>
                </td>
            </tr>
        </thead>
        <tbody>
     
            <?php
            $args = array(
              'orderby' => 'name',
              'order' => 'ASC',
              'parent' => '9',
              );
            $categories = get_categories($args);
            foreach($categories as $category) { ?>
                <?php $catFilter = $category->name; 
                      $catFilter = str_replace(' ', '', $catFilter);  ?>
                <tr>
                    <td class="filter" data-filter=".<?php echo $catFilter ?>">
                        <p><?php echo $category->name ?></p>
                    </td>
                </tr>
            <?php } ?>             
        </tbody>
        </table>
    </div>
    <?php endif; ?>


    <div class="archive-list" id="Container">
            <?php if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb('<p id="breadcrumbs">','</p>');
            } ?>    
        <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php $cat = get_the_category(); 
                  $catClass = $cat[0]->cat_name;
                  $catClass = str_replace(' ', '', $catClass);
            ?>
            <div class="archive-list-article mix <?php echo $catClass ?>">
            <?php if ( wp_get_attachment_url( get_post_thumbnail_id($post->ID) )): ?>
                <div class="title">
                    <header>
                        <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                        </a>
                        
                    </header>

                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <img src="<?php echo $url; ?>">
                </div>

                <div class="postcontent">
            <?php else: ?>
                <div class="postcontent">
                <header>
                    <a href="<?php the_permalink(); ?>">
                    <h3><?php the_title(); ?></h3>
                    </a>
                </header>                  
            <?php endif; ?>  
                
                    <?php the_content(__('(continue...)')); ?>
                </div><!--/postcontent-->
    
            </div><!--/post-->

        <?php endwhile; endif; ?>
    </div>
</section>
<?php get_footer(); ?>