<?php
/**
 * Copyright 2013 Nelio Software S.L.
 * This script is distributed under the terms of the GNU General Public License.
 *
 * This script is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License.
 * This script is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


if ( !class_exists( 'NelioABErrorPage' ) ) {

	require_once( NELIOAB_UTILS_DIR . '/admin-ajax-page.php' );

	class NelioABErrorPage extends NelioABAdminAjaxPage {

		private $msg;

		public function __construct( $msg ) {
			parent::__construct( __( 'Error', 'nelioab' ) );
			$this->msg = $msg;
		}

		protected function do_render() {
			echo '<center>';
			echo sprintf( '<img src="%s" alt="%s" />',
				nelioab_asset_link( '/admin/images/error-icon.png' ),
				__( 'Funny image to graphically notify of an error.', 'nelioab' )
			);
			echo "<h2>$this->msg</h2>";
			echo '</center>';
		}

	}//NelioABErrorPage

}

?>
